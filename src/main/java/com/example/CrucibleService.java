package com.example;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

public class CrucibleService {
	
	public String getResponse(String url){
		String text = "";
		try{
			Document doc = Jsoup.connect(url).get();
			text = "Latest Comment :"+ doc.getElementsByClass("latest-comment").text();
		}
		
		catch(Exception e){
			e.printStackTrace();
		}
		return text;
	}

}
