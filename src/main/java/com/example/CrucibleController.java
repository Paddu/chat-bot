package com.example;



import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

@ResponseBody
@RestController
public class CrucibleController {

	@RequestMapping("/hello")
	public String hello(@RequestParam String name) {
		return "Hello "+name;
	}
	
	
	@RequestMapping("/Crucible")
	public Description getDescription(@RequestBody CruciblePOJO cruPojo){
		Description description = new Description();
		CrucibleService service = new CrucibleService();
		description.setDescription(service.getResponse(cruPojo.getUrl()));
		return description;
		
	}
}

